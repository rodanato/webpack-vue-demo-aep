
//VUE
import Vue from 'vue'


//VUE LIBRARIES
import VueResource from '../node_modules/vue-resource/dist/vue-resource'


//VUE COMPONENTS
import AepHeader from './js/components/header.vue'
import Vuetable from './js/components/Vuetable.vue'
import VuetablePagination from './js/components/VuetablePaginationBootstrap.vue'
import PaginationMixin from './js/components/VuetablePaginationMixin.vue'


Vue.use(VueResource);

Vue.component('vuetable', Vuetable)
Vue.component('vuetable-pagination', VuetablePagination)
Vue.component('aep-header', AepHeader)



var exampleColumns = [{
  name: 'name',
  title: 'Name',
  sortField: 'name'
}, {
  name: 'nickname',
  title: 'Nickname',
  sortField: 'nickname'
}, {
  name: 'email',
  title: 'Email',
  sortField: 'email'
}, {
  name: 'birthdate',
  title: 'Birthdate',
  sortField: 'birthdate'
}, {
  name: 'gender',
  title: 'Gender',
  sortField: 'gender',
  dataClass: 'text-center'
}, {
  name: 'group_id',
  title: 'Group id',
  sortField: 'group_id',
  dataClass: 'text-center'
}, {
  name: 'created_at',
  title: 'Created at',
  sortField: 'created_at'
}, {
  name: 'updated_at',
  title: 'Updated at',
  sortField: 'updated_at'
}, {
  name: '__actions',
  dataClass: 'text-center'
}];


const vm = new Vue({
  el: '#main',
  data: {
    apiUrl: 'http://vuetable.ratiw.net/api/users',
    activeSearch: true,
    sortOrder: {
      field: 'name',
      direction: 'asc'
    },
    perPage: 5,
    columns: exampleColumns,
    itemActions: [{
      name: 'view-item',
      label: '',
      icon: 'glyphicon glyphicon-zoom-in',
      class: 'btn btn-info'
    }, {
      name: 'edit-item',
      label: '',
      icon: 'glyphicon glyphicon-pencil',
      class: 'btn btn-warning'
    }, {
      name: 'delete-item',
      label: '',
      icon: 'glyphicon glyphicon-remove',
      class: 'btn btn-danger'
    }]
  },
  events: {
    'vuetable:action': function(action, data) {
      if (action == 'view-item') {
        console.log(action, data.nickname)
      } else if (action == 'edit-item') {
        console.log(action, data.nickname)
      } else if (action == 'delete-item') {
        console.log(action, data.nickname)
      }
    }
  }
})