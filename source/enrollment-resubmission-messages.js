import Vue from 'vue'


//VUE COMPONENTS
import AepHeader from './js/components/header.vue'
import AepNotification from './js/components/notification.vue'

Vue.component('aep-header', AepHeader)
Vue.component('aep-notification', AepNotification)


const vm = new Vue({
  el: '#main',
  data: {
    'notification1': {
      title: 'Thank you!',
      message: "Thank you for your interest in AEP Energy. We are currently processing your request. You may create an account or login <a href='https://aepenergy.com/enrollment/login/existingUserLogin.html' class='link'>here</a> .<br> Additional information can be viewed as it becomes available.",
      type: 'success'
    },
    'notification2': {
      title: 'Unable to complete your request',
      message: "We're sorry, but we were unable to complete you request.<br> Please contact our Customer Care Team for further assistance by calling 1-866-258-3782.",
      type: 'warning'
    },
    'notification3': {
      title: 'Unable to complete your request',
      message: "We're sorry, but we were unable to complete you request. Please login <a href='https://aepenergy.com/enrollment/login/existingUserLogin.html' class='link'>here</a> and try again.<br> If your continue to experience these issues, please contact our Customer Care Team by calling 1-866-258-3782.",
      type: 'warning'     
    },
    'notification4': {
      title: 'Unable to complete your request',
      message: "We're sorry, but the offer you selected is no longer available.<br> Please click <a href='https://aepenergy.com/enrollment/enrollment.html' class='link'>here</a> to view current offers in your area.",
      type: 'warning'     
    }
  }
})
