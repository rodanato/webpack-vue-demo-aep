
//VUE
import Vue from 'vue'


//VUE LIBRARIES
import VueResource from '../node_modules/vue-resource/dist/vue-resource'
import VueValidator from '../node_modules/vue-validator/dist/vue-validator'


//JS LIBRARIES
import bootstrap from '../node_modules/bootstrap/dist/js/bootstrap'


//VUE COMPONENTS
import AepHeader from './js/components/header.vue'
import AepFormError from './js/components/form-error.vue'
import AepImageModal from './js/components/image-modal.vue'


Vue.component('aep-header', AepHeader)
Vue.component('aep-image-modal', AepImageModal)
Vue.component('aep-form-error', AepFormError)
Vue.use(VueResource);
Vue.use(VueValidator)




var invoiceImg = '<img class="img-responsive" src="' + require("./images/invoiceComed.jpg") + '" >';


const vm = new Vue({
  el: '#main',
  data: {
    error_messages: '',
    invoiceImg: invoiceImg
  },
  created: function() {
    this.$http.get('data.json').then(function(response) {
      this.$set('error_messages', response.data.error_messages)
    }, function(error) {
      console.log(error);
    });
  }
})

