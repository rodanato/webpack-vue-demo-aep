var path = require('path')
var webpack = require('webpack')
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var CommonsPlugin = new require("webpack/lib/optimize/CommonsChunkPlugin")



module.exports = {
  entry: {
    'checkout': "./source/checkout.js",
    'congratulations': "./source/congratulations.js",
    'enrollment-resubmission-messages': "./source/enrollment-resubmission-messages.js",
    'enrollment-resubmission': "./source/enrollment-resubmission.js",
    'common': ["./source/common.js"]
  },
  output: {
    path: './output',
    publicPath: './output/',
    filename: 'js/[name].js'
  },
  // devtool: '#eval-source-map',
  resolveLoader: {
    root: path.join(__dirname, 'node_modules')
  },
  module: {
    loaders: [{
      test: /\.css$/,
      loader: ExtractTextPlugin.extract("style-loader", "css-loader")
    }, {
      test: /\.styl$/,
      // loader: ExtractTextPlugin.extract("style-loader", "css!stylus?resolve url")
      loader: ExtractTextPlugin.extract('style!css!stylus?resolve url')
    }, {
      test: /\.vue$/,
      loader: 'vue'
    }, {
      test: /\.js$/,
      loader: 'babel',
      exclude: /node_modules/
    }, {
      test: /\.json$/,
      loader: 'json'
    }, {
      test: /\.html$/,
      loader: 'vue-html'
    }, {
      test: /\.((eot|ttf|woff|woff2|svg)(\?v=[0-9]\.[0-9]\.[0-9]))|(eot|ttf|woff|woff2|svg)$/,
      loader: 'url'
    }, {
      test: /\.(jpe?g|png|gif|ico)$/,
      loader: 'file?name=images/[name].[ext]'
    }]
  },
  stylus: {
    use: [require('nib')()],
    import: ['~nib/lib/nib/index.styl']
  },
  plugins: [
    new ExtractTextPlugin('css/bundle.css'),
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      "window.jQuery": 'jquery'
    }),
    new CommonsPlugin({
      minChunks: 3,
      name: "common"
    })
  ],
  devServer: {
    historyApiFallback: true,
    noInfo: true
  }
}

if (process.env.NODE_ENV === 'production') {
  // module.exports.devtool = '#source-map'
  module.exports.plugins = (module.exports.plugins || []).concat([
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: '"production"'
      }
    }),
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false
      }
    }),
    new webpack.optimize.OccurenceOrderPlugin()
  ])
}